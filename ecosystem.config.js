module.exports = {
	apps : [{
		name      : 'bot/removalAlert',
		script    : 'build/removalAlert.js',
		env: {
			// Either inject the values via environment variables or define them here
            TRANSPORT_BIND_ADDRESS: process.env.TRANSPORT_BIND_ADDRESS || "",
            BOT_REMOVALALERT_SUBREDDIT: process.env.BOT_REMOVALALERT_SUBREDDIT || "",
            BOT_REMOVALALERT_THRESHOLD: process.env.BOT_REMOVALALERT_THRESHOLD || "",
            BOT_REMOVALALERT_CHANNEL: process.env.BOT_REMOVALALERT_CHANNEL || "",
            DB_HOST: process.env.DB_HOST || "",
            DB_PORT: process.env.DB_PORT || "",
            DB_USERNAME: process.env.DB_USERNAME || "",
            DB_PASSWORD: process.env.DB_PASSWORD || "",
            DB_NAME: process.env.DB_NAME || ""
		}
	}]
};
