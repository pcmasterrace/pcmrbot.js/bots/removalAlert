import { Service, Errors } from "moleculer";
import * as moment from "moment";

class RemovalAlertsService extends Service {
	constructor(broker) {
		super(broker);

		this.parseServiceSchema({
			name: "bot.removalAlert",
			version: 2,
			dependencies: [
				{name: "reddit.post", version: 1},
				{name: "reddit-rt.modlog", version: 1},
				{name: "slack.web", version: 1},
				{name: "utilities.text", version: 1}
			],
			events: {
				"reddit-rt.modlog.removelink": this.eventHandler
			},
			created: this.serviceCreated
		})
	}

	async eventHandler(modlogEntry) {
		let post = await this.broker.call("v1.reddit.post.getPost", {postId: modlogEntry.target_fullname});
		
		if(post.score >= Number(process.env.BOT_REMOVALALERT_THRESHOLD)) {
			let time = moment.utc(modlogEntry.created_utc * 1000);
			let alertMessage = `*Notice*: <https://www.reddit.com/u/${modlogEntry.mod}|${modlogEntry.mod}> removed this post at <!date^${time.unix()}^{time} on {date}|${time.format("LTS")} UTC on ${time.format("LL")}>`
			let selftext = post.selftext !== "" ? await this.broker.call("v1.utilities.text.markdownToMrkdwn", {string: post.selftext}) : undefined;

			let attachments = [{
				fallback: `https://www.reddit.com${modlogEntry.target_permalink}`,
				author: post.author,
				author_link: `https://www.reddit.com/user/${post.author}`,
				title: `[${post.score} upvotes] ${post.title}`,
				title_link: `https://www.reddit.com${modlogEntry.target_permalink}`,
				image_url: post.preview !== undefined ? post.preview.images[0].source.url : undefined,
				text: selftext,
				footer: `/r/${post.subreddit}`
			}];

			await this.broker.call("v1.slack.web.postMessage", {
				message: alertMessage,
				attachments: attachments, 
				channel: process.env.BOT_REMOVALALERT_CHANNEL
			});
		}
	}

	async serviceCreated() {
		if(process.env.BOT_REMOVALALERT_CHANNEL === "") throw new Errors.MoleculerError("No removal alert channel defined, you dingus");
		if(process.env.BOT_REMOVALALERT_THRESHOLD === "") throw new Errors.MoleculerError("No removal alert threshold defined, you dingus");
	}
}

module.exports = RemovalAlertsService;